# ---------------------------------------------- #
#               Auth to providers                #
# ---------------------------------------------- #

# Define VK Cloud terraform provider
terraform {
  backend "s3" {
    bucket                      = "project-bucket"
    region                      = "us-east-1"
    key                         = "terraform.tfstate"
    endpoint                    = "https://hb.bizmrg.com/"
    skip_credentials_validation = true
  }
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"
      version = "~> 0.1.12"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

# Authenticate to VK Cloud tenant
provider "vkcs" {
  region   = "RegionOne"
  auth_url = "https://infra.mail.ru:35357/v3/"
}

# Authenticate to Cloudflare provider
provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

# Create a keypair for product deployment
resource "vkcs_compute_keypair" "keypair" {
  name = var.keypair_name
}

# Save keypair for provisioner
resource "local_file" "keypair" {
  content         = <<-EOL
    ${vkcs_compute_keypair.keypair.private_key}
    EOL
  filename        = "${path.module}/privatekey.pem"
  file_permission = 400
}
