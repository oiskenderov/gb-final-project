# Скрипты инициализации приложений

## app-init.sh
Скрипт запускается вместе с созданием пула виртуальных машин для statelss-приложений.
Основные задачи скрипта:
- установка и конфигурирование NginX
- установка и конфигурирование клиента NFS
- установка и конфигурирование PHP-MySQL, Wordpress
- установка и конфигурирование метрик NodeExporter, NginX Exporter

## nfs-init.sh
Скрит запускается при создании виртуальной машины NFS01
Задачи скрипта:
- установка и конфигурирование сервера NFS
- установка и конфигурирование метрик NodeExporter

## mon-init.sh
Скрит запускается при создании виртуальной машины NFS01
Задачи скрипта:
- установка и конфигурирование Prometheus+Grafana
- добавление дашбордов через provisioning
- добавление targets для Prometheus
- установка и конфигурирование Alertmanager
- добавление базовового алерта
- установка и конфигурирование метрик NodeExporter
