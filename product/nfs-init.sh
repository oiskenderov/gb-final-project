#!/bin/bash

# Update the package manager
sudo apt update

# ---------------------------------------------- #
#          Install NetworkFileStorage            #
# ---------------------------------------------- #

# Install NFS server
sudo apt install nfs-kernel-server -y

# Create a directory to share
sudo mkdir /wordpress

# Set permissions for the directory
sudo chown nobody:nogroup /wordpress
sudo chmod 777 /wordpress

# Configure the NFS share
echo "/wordpress *(rw,sync,no_subtree_check)" | sudo tee -a /etc/exports

# Restart the NFS server
sudo systemctl restart nfs-kernel-server

# ---------------------------------------------- #
#             Install NodeExporter               #
# ---------------------------------------------- #

# Make node_exporter user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter

# Download node_exporter and copy utilities to where they should be in the filesystem
VERSION=$(curl https://raw.githubusercontent.com/prometheus/node_exporter/master/VERSION)
wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_VERSION}/node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
tar xvzf node_exporter-${NODE_VERSION}.linux-amd64.tar.gz

sudo cp node_exporter-${NODE_VERSION}.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter

# systemd
sudo echo '
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter

# Installation cleanup
rm node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
rm -rf node_exporter-${NODE_VERSION}.linux-amd64